
//RatpackServer
import static ratpack.groovy.Groovy.ratpack
import groovy.json.*
import ratpack.jackson.Jackson
import groovy.util.logging.Slf4j


//list of ip addresses with limit 5
def ip = ['8.8.8.8', '192.0.1.1','180.0.0.0', '177.0.0.0', '190.0.0.0']
int count = 0

//map of country
countryList = [:]
countryList ['northcountries'] = []

if  (ip.size()<=5) {
    while (count<ip.size()) {
        //URL
        def get = new URL("https://ipvigilante.com/json/${ip[count]}").openConnection()
        //Get response code
        def rc = get.getResponseCode()
        //Get response
        def response = get.getInputStream().getText()

        //Convert to JSON object
        def parser = new JsonSlurper()
        def j = parser.parseText(response)
        def lati = j.data.latitude
        double latiDouble = Double.parseDouble(lati)

        //check response code and return countries in north hemisphere
        if (rc.equals(200)) {
            //check if latitude in the northern hemisphere between 0.0000 and 90.0000
            if (latiDouble >= 00.0000 && latiDouble <= 90.9999) {
                def country = j.data.country_name
                //println(country)
                //if (!(countryList.containsValue(country)))
                countryList['northcountries'].add(country)
            }
        } else {
            println("sorry try again!")
        }

        count += 1
    }
}
else {
    println("sorry try again 1 to 5 IP addresses!")
}
def asJson = JsonOutput.toJson(countryList['northcountries'].toSet().sort())

//custom render, can include ip too
/*
public void render (GroovyContext context, def countryList) throws Exception {
            context.getResponse().send(countryList);
}*/

ratpack {
    serverConfig {
        port(8888)
    }

    handlers {
        get() {
            //custom render
            render (Jackson.json(asJson))
        }
    }
}